
I
Ciña ¡Oh Patria! tus sienes de oliva
De la paz el arcángel divino,
Que en el cielo tu eterno destino
Por el dedo de Dios se escribió.
Mas si osare un extraño enemigo
Profanar con su planta tu suelo,
Piensa ¡Oh Patria querida! que el cielo
Un soldado en cada hijo te dio.

V
¡Guerra, guerra sin tregua al que intente
De la patria manchar los blasones!,
¡Guerra, guerra! los patrios pendones
En las olas de sangre empapad.
¡Guerra, guerra! en el monte, en el valle,
Los cañones horrísonos truenen
Y los ecos sonoros resuenen
Con las voces de ¡Unión! ¡Libertad!






VI
Antes, Patria, que inermes tus hijos
Bajo el yugo su cuello dobleguen,
Tus campiñas con sangre se rieguen,
Sobre sangre se estampe su pie.
Y tus templos, palacios y torres
Se derrumben con hórrido estruendo,
Y sus ruinas existan diciendo:
De mil héroes la patria aquí fue.


X
¡Patria! ¡Patria! tus hijos te juran
Exhalar en tus aras su aliento,
Si el clarín con su bélico acento
Los convoca a lidiar con valor.
¡Para ti las guirnaldas de oliva!
¡Un recuerdo para ellos de gloria!
¡Un laurel para ti de victoria!
¡Un sepulcro para ellos de honor!
